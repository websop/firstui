## Using Bootstrap with Bower

To deploy a UI project to obtain our UI template `bower` and `gulp` to compile Bootstrap, so we’ll need [Node.js](http://nodejs.org/).

First I made a `package.json` file for install Node. Either use `npm init -y` or create a `package.json` 

To install `gulp`, `gulp-bower`, and `gulp-sass`:

```shell
$ npm install --save-dev gulp gulp-bower gulp-sass
```

`package.json` file should now look something like this:

```json
{
  "devDependencies": {
    "gulp": "^3.8.11",
    "gulp-bower": "0.0.10",
    "gulp-sass": "^1.3.3"
  }
}
```

Now let’s use Bower to install bootstrap. This will allow us to import Bootstrap into our SCSS code and compile it down to CSS manually.

Create a `bower.json` file using `bower init` or by manually creating one:

```json
{
  "name": "bootstrap-sass",
  "description": "",
  "authors": [
    ""
  ],
  "license": "ISC",
  "homepage": "",
  "private": true,
  "ignore": [
    "**/.*",
    "node_modules",
    "bower_components",
    "test",
    "tests"
  ],
  "dependencies": {
    "bootstrap-sass": "^3.3.7"
  }
}
```

Now let’s install `bootstrap-sass` with Bower.

```shell
$ bower install --save bootstrap-sass
```

Our `bower.json` file should now have `bootstrap-sass` listed as a dependency:

Now we can make an SCSS file that includes bootstrap into our project. Let’s call our SCSS file `css/app.scss`:

```scss
@import "bootstrap";
@import "bootstrap/theme";
```

Now let’s use gulp to compile our `app.scss` which includes Bootstrap SASS:

```javascript
var gulp = require('gulp');
var sass = require('gulp-sass');

var config = {
    bootstrapDir: './bower_components/bootstrap-sass',
    publicDir: './public',
};

gulp.task('css', function() {
    return gulp.src('./css/app.scss')
    .pipe(sass({
        includePaths: [config.bootstrapDir + '/assets/stylesheets'],
    }))
    .pipe(gulp.dest(config.publicDir + '/css'));
});

gulp.task('fonts', function() {
    return gulp.src(config.bootstrapDir + '/assets/fonts/**/*')
    .pipe(gulp.dest(config.publicDir + '/fonts'));
});

gulp.task('default', ['css', 'fonts']);
```

Now when we run `gulp`, our compiled Bootstrap CSS should appear in the `public/css` directory

