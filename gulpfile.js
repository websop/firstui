var gulp = require('gulp');
	  sass = require('gulp-sass');
    htmlrender = require('gulp-htmlrender');
    // spawn = require('child-process').span;    
    browserSync = require('browser-sync').create();    
    reload  = browserSync.reload;
    save = require('gulp-save');
    del = require('del');
    runSequence = require('run-sequence');    

const changed = require('gulp-changed');

var config = {
    bootstrapDir: './bower_components/bootstrap-sass',
    publicDir: './public',
};

gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: './public'
    },
  })
})

gulp.task('render', function() {
     return gulp.src('./src/*.html', {read: false})
    .pipe(htmlrender.render())
    .pipe(changed('only', {extension: '.html'}))
    .pipe(save('clearcache'))    
    .pipe(gulp.dest(config.publicDir))
    .pipe(save.restore('clearcache'))
    .pipe(browserSync.reload({
      stream: true
    }));
});

gulp.task('sass', function() {
    return gulp.src('./scss/*.scss')
    .pipe(sass({
        includePaths: [config.bootstrapDir + '/assets/stylesheets']
    }))
    .pipe(changed('only', {extension: '.css'}))
    .pipe(save('clearcache'))
    .pipe(gulp.dest(config.publicDir + '/css'))
    .pipe(save.clear('clearcache'))
    .pipe(browserSync.reload({
      stream: true
    }));
});

gulp.task('auto-regulp', function() {
    spawn('gulp', [], {stdio: 'inherit'});
    process.exit();
});

gulp.task('watch', function () {
  // gulp.watch('gulpfile.js', ['auto-reload']);  
  gulp.watch('gulpfile.js').on('change', browserSync.reload);
  gulp.watch('scss/*.scss', ['sass']);
  gulp.watch(['./src/*.html', './src/**/*.html'], ['render']);
  // Other watchers
});

gulp.task('fonts', function() {
    return gulp.src(config.bootstrapDir + '/assets/fonts/**/*')
    .pipe(gulp.dest(config.publicDir + '/fonts'));
});

gulp.task('clean:public', function() {
  return del.sync('public');
})

gulp.task('build', function (callback) {
  runSequence('clean:public', 
             ['sass', 'render', 'fonts'],
             ['browserSync', 'watch'],
    callback
  )
})

gulp.task('default', function (callback) {
  runSequence(['sass', 'render', 'browserSync', 'watch'],
    callback
  )
})
